document.getElementById("val1").addEventListener("change",compute,false);
document.getElementById("val2").addEventListener("change",compute,false);
document.getElementById("+").addEventListener("click",compute,false);
document.getElementById("-").addEventListener("click",compute,false);
document.getElementById("*").addEventListener("click",compute,false);
document.getElementById("/").addEventListener("click",compute,false);


function compute(){
	var options = document.getElementsByName("option");
	var checked = false;
	for(var i=0;i<options.length;i++){
		if(options[i].checked){
			checked = true;
			break;
		}
	}

	var val1 = document.getElementById("val1").value;
	var val2 = document.getElementById("val2").value;

	if(val1.match(/^\d+$/) && val2.match(/^\d+$/) && checked){
		var result = null;
		val1 = parseInt(val1);
		val2 = parseInt(val2);
		if (document.getElementById("+").checked){
			result = val1 + val2;
		}
		else if (document.getElementById("-").checked){
			result = val1 - val2;
		}
		else if (document.getElementById("*").checked){
			result = val1 * val2;
		}
		else if (document.getElementById("/").checked){
			if (val2 != 0){
				result = val1/val2;
			}
			else{
				result = "invalid denominator";
			}
		}
		document.getElementById("result").innerHTML = result;
	}
	else{
		document.getElementById("result").innerHTML = "invalid input";
	}
}
